export type Image =  {
  url: string;
  tag: string;
  id: string;
}