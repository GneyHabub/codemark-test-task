import React, {useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import fetchImage from './fetchImage';
import {Spinner} from 'react-bootstrap';
import { Image } from './types';
import ImageCard from './ImageCard'
import ErrorModal from './ErrorModal';
import ImageGroupWrapper from './ImageGroupWrapper';

var runDelay = false;

function App() {
  const randomTags = ['cat', 'dog', 'monkey', 'donkey', 'putin', 'trump', 'ynwa', 'anime'];
  const regExp = new RegExp('^[a-zA-Z, ]*$');
  var [tag, setTag] = useState('');
  var [showToast, setShowToast] = useState(false);
  var [groupImages, setGroupImages] = useState(false);
  var [modalMessage, setModalMessage] = useState('');
  var [isLoading, setIsLoading] = useState(false);
  var [images, setImages]: [Image[][], Function] = useState([]);

  async function handleClick(e: React.MouseEvent<Element, MouseEvent>) {
    e.preventDefault();
    runDelay = false;
    if (tag.trim() === '') {
      setShowToast(true);
      setModalMessage('Empty tag!');
      return;
    } else if(!regExp.test(tag.toLowerCase())) {
      setShowToast(true);
      setModalMessage('Wrong tag provided!');
      return;
    } else if(tag.toLowerCase().trim() === 'delay') {
      runDelay = true;
      delay();
      return;
    }
    var res = await fetchImage(tag, setIsLoading);
    if(!res.error) {
      setImages([...images, res.images]);
    } else {
      setShowToast(true);
      setModalMessage('No GIFs found!');
    }
  }

  function delay() {
    var interval = setInterval(async () => {
      if(!runDelay) {
        clearInterval(interval);
      } else {
        var res = await fetchImage(randomTags[Math.floor(Math.random() * randomTags.length)], setIsLoading);
        setImages((prev: Image[][]) => [...prev, res.images]);
      }
    }, 5000);
  }

  return (
    <div className="App">
      <div className="search-pannel-wrapper">
        <form className="search-pannel">
          <input
            onChange={(e) => setTag(e.target.value)} 
            value={tag} 
            name="tag"
            type="text"
          ></input>
        
          <button disabled={isLoading} type="submit" onClick={handleClick}>Get Image</button>

          <button onClick={(e) => {
            e.preventDefault();
            setImages([]); 
            setTag('');
            runDelay = false;
          }}>Clear All</button>

          <button onClick={(e) => {
            e.preventDefault();
            setGroupImages(!groupImages)
          }}>{groupImages ? 'Ungroup' : 'Group'}</button>
            
        </form>
      </div>
      
      {
       groupImages ? 
        <ImageGroupWrapper onImageClick={setTag}  images={images}></ImageGroupWrapper> : 
        <div className="images-block">
          {images.map((el: Image[], idx: number) => <ImageCard onImageClick={setTag} images={el} key={idx} ></ImageCard>)}
          {isLoading? <Spinner animation="border"></Spinner> : null}
        </div>
      }

      <ErrorModal showToast={showToast} setShowToast={setShowToast} message={modalMessage}></ErrorModal>

    </div>
  );
}

export default App;
