import React from 'react'
import {Modal} from 'react-bootstrap';

export default function ErrorModal({showToast, setShowToast, message}: {showToast: boolean, setShowToast: Function, message: string}) {
  return (
    <div>
      <Modal animation={false} show={showToast} onHide={() => setShowToast(!showToast)}>
        <Modal.Header closeButton>
          <strong className="mr-auto">Wrong Tag!</strong>
        </Modal.Header>
          <Modal.Body>{message}</Modal.Body>
      </Modal>
    </div>
  )
}
