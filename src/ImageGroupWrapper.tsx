import React from 'react';
import {Image} from './types';
import ImageCard from './ImageCard'

export default function ImageGroupWrapper({images, onImageClick}: {images: Image[][], onImageClick: (tag: string) => void}) {
  var groups: {[key: string]: Image[][]} = {};
  images.forEach(imageCollection => {
    imageCollection.forEach(img => {
      groups[img.tag] ? groups[img.tag].push(imageCollection) : groups[img.tag] = [imageCollection];
    });
  });
  return (
    <div>
      {Object.keys(groups).map(group => { return (
        <div>
          <h1 className="group-title">{group}</h1>
          <hr></hr>
          {groups[group].map(image => (
              <ImageCard images={image} onImageClick={onImageClick}/>
            )
          )}
        </div>);
      })}
    </div>
  )
}
