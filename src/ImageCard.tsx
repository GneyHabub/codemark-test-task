import React from 'react'
import { Image } from './types';

export default function ImageCard(props: {images: Image[], onImageClick: (tag: string) => void}) {
  var {images, onImageClick} = props;
  var imageBlocks = images.map(img => (
    <img 
      onClick={(e: React.MouseEvent<Element, MouseEvent>) => {e.preventDefault(); onImageClick(img.tag);}} 
      key={img.id} 
      src={img.url} 
      alt={img.tag}
    />
  ))
  return (
    <div className="image-wrapper" >
      {imageBlocks}
    </div>
  );
}
