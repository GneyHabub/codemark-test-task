import axios from 'axios';
import { Image } from './types';

export default async function fetchImage (tag: string, setIsLoading: Function): Promise<{error: boolean, images: Image[]}> {
  var tags = tag.split(',').map(el => el.trim());
  var images: Image[] = [];
  var error = false;

  setIsLoading(true);
  var requests = tags.map(tag => axios.get('https://api.giphy.com/v1/gifs/random', {params: {api_key: 'gTJAO48YcpmrADUyo4opy4ES4g7iDBxx', tag}}));
  await Promise.all(requests).then(responses => {
    responses.forEach((resp, idx) => {
      if (resp.data.data.length === 0) {
        error = true;
      }
      images.push({id: resp.data.data.id, tag: tags[idx], url: resp.data.data.image_url})
    })
  });
  setIsLoading(false);

  if (error) {
    return {error: true, images}
  }
  return {error: false, images}
}
